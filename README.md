# pace-2019-vc

## Build

```
make
```

To do a debug build with logs, stderr output and profiling
```
make debug
```
Logs are appended to a `runner.log` file

## Run
```
./Runner
```
Reads the graph file from stdin, prints the resulting vertex cover to stdout, common usage:
```
./Runner < graphfile.gr > vertexcoverfile.vc
```

### Tester tool

```
./tester.py
```
Runs `Runner` consequently on all tests from `vc-exact-public` with a 30 minute timeout, uses `vc_validate` to validate the correctness
