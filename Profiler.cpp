#include "Profiler.h"
#include "Macro.h"

namespace profile {
    Profiler::Profiler(const char* name) : time(0), calls(0), name(name), inUse(false) {}

    Profiler::~Profiler() {
        TO_LOG("[PROFILE] %20s calls: %5lld, total: %.4f, average: %lldmus\n", name, calls, time.count(), (long long)(1000000 * time.count() / calls));
    }

    bool Profiler::startCall() {
        calls++;
        if (inUse) {
            return false;
        } else {
            inUse = true;
            return true;
        }
    }

    void Profiler::addTime(std::chrono::duration<double> toAdd) {
        time += toAdd;
        inUse = false;
    }

    ProfilerInstance::ProfilerInstance(Profiler& profiler) {
        if (profiler.startCall()) {
            profilerPtr = &profiler;
            start = std::chrono::steady_clock::now();
        } else {
            profilerPtr = nullptr;
        }
    }

    ProfilerInstance::~ProfilerInstance() {
        if (profilerPtr != nullptr) {
            profilerPtr->addTime(std::chrono::steady_clock::now() - start);
        }
    }

    EventCounter::EventCounter(const char* name) : count(0), name(name) {}

    EventCounter::~EventCounter() {
        TO_LOG("[COUNT] %20s count %5lld\n", name, count);
    }
}
