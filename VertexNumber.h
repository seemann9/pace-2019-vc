#pragma once

using VertexNumber = int;
using VertexList = std::vector<VertexNumber>;

const int INF = 1e9;
