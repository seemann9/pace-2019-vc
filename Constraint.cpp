#include "Constraint.h"

bool Constraint::isAlive() const {
    return terms.size() > std::max(0, upperBound);
}

void Constraint::deleteVertex(VertexNumber vertex) {
    for (int i = int(terms.size()) - 1; i >= 0; --i) {
        if (terms[i].vertex == vertex) {
            terms.erase(terms.begin() + i);
        }
    }
}

void Constraint::fixVertex(VertexNumber vertex, bool inVC) {
    for (int i = int(terms.size()) - 1; i >= 0; --i) {
        if (terms[i].vertex == vertex) {
            if (inVC) {
                upperBound -= terms[i].coefficient;
            } else {
                upperBound -= (1 - terms[i].coefficient);
            }

            terms.erase(terms.begin() + i);
        }
    }
}

void Constraint::replaceVertex(VertexNumber vertex, VertexNumber newVertex, bool flipped) {
    for (int i = 0; i < terms.size(); ++i) {
        if (terms[i].vertex == vertex) {
            terms[i].vertex = newVertex;
            if (flipped) {
                terms[i].coefficient = 1 - terms[i].coefficient;
            }
        }
    }
}
