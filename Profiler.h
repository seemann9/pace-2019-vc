#include <chrono>

#pragma once

namespace profile {
    class Profiler {
    public:
        Profiler(const char* name);
        ~Profiler();
        bool startCall();
        void addTime(std::chrono::duration<double> toAdd);

    private:
        std::chrono::duration<double> time;
        long long calls;
        const char* name;
        bool inUse;
    };


    class ProfilerInstance {
    public:
        ProfilerInstance(Profiler& profiler);
        ~ProfilerInstance();

    private:
        Profiler* profilerPtr;
        std::chrono::time_point<std::chrono::steady_clock> start;
    };

    class EventCounter {
    public:
        EventCounter(const char* name);
        ~EventCounter();

        long long count;
    private:
        const char* name;
    };
}

#ifdef DEBUG
#define PROFILE_SECTION(name) \
    static profile::Profiler profiler_##name(#name ""); \
    profile::ProfilerInstance profilerInstance_##name(profiler_##name);

#define COUNT(name, amount) \
    static profile::EventCounter counter_##name(#name ""); \
    counter_##name.count += amount;
#else
#define PROFILE_SECTION(name)
#define COUNT(name, condition)
#endif
