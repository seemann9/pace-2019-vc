#!/usr/bin/python3

import time
import subprocess
import glob
from datetime import datetime
import sys
from os import system
commitName = subprocess.Popen(["git", "log", "--pretty=%B", "--no-merges", "-1"], stdout=subprocess.PIPE).stdout.readline().decode("utf-8").strip()

suff = '_' + str(commitName) + '_' +  datetime.now().strftime('%Y-%m-%d %H:%M:%S')
suff = suff.replace(' ', '_')

out = open('tester' + suff + '.log', 'w')
for test in sorted(glob.glob('vc-exact-public/vc-exact_???.gr')):
    runnerLog = 'runner' + suff + '.log'
    log = open(runnerLog, 'a')
    print(test, file=log)
    log.close()
    print(test)
    ans = test.split('.')[0] + '.vc'
    ansSize = -1
    if len(glob.glob(ans)) > 0:
        fline = open(ans, 'r').readline().split()
        if len(fline) > 3:
            ansSize = int(fline[3])
    try:
        start = time.time()
        subprocess.run(['./Runner', runnerLog], stdin=open(test, 'r'), stdout=open("out", 'w'), timeout=30 * 60);
        elapsed = time.time() - start
        res = ('%.2f' % elapsed)
        newAnsSize = int(open("out", 'r').readline().split()[3])
        validation = subprocess.run(['vc_validate/bin/vc_validate', '-g', test, '-vc',"out"])
        if validation.returncode != 0:
            res += ', not a VC'
            if ansSize == -1 or ansSize > newAnsSize:
                print("New VC")
                system('cp out ' + ans)
            elif ansSize > newAnsSize:
                res += ', wrong size: %d vs %d' % (ansSize, newAnsSize)
    except subprocess.TimeoutExpired:
        res = 'TL'

    print(res)
    print(test, res, file=out)
    out.flush()

