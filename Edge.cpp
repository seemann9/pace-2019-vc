#include "Edge.h"

Edge::Edge() : start(0), end(0) {};

Edge::Edge(int start, int end) : start(start), end(end) {}

bool Edge::operator < (const Edge& other) const {
    if (start != other.start) {
        return start < other.start;
    }
    return end < other.end;
}

bool Edge::operator == (const Edge& other) const {
    return start == other.start && end == other.end;
}

int Edge::getStart() const {
    return start;
}

int Edge::getEnd() const {
    return end;
}
