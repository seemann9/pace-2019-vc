#pragma once

class Edge {
public:
    Edge();
    Edge(int start, int end);
    bool operator < (const Edge& other) const;
    bool operator == (const Edge& other) const;
    int getStart() const;
    int getEnd() const;
private:
    int start, end;
};
