## Requirements

* `g++` compiler, version >= 7.2.0 [version >= 5.3.1 is enough if `-std=c++17` is replaced with `-std=c++11` in `Makefile`/`CMakeLists.txt`]
* GNU `make`, version >= 4.1
* `python3`, version >= 3.5 [optional, for `tester.py`]
* `cmake`, version >= 3.0 [optional, premade `Makefile` is supplied]

No extra libraries are used
