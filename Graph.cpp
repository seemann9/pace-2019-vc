#include <cassert>
#include <algorithm>
#include <queue>
#include "Macro.h"
#include "Profiler.h"
#include "ListTools.h"
#include "Graph.h"
#include "Solver.h"

const VertexNumber Graph::MERGED_LABEL = -1;

Graph::Iterator::Iterator(const Graph* graph, VertexNumber i) : graph(graph), i(i) {
    skipDeleted();
}

Graph::Iterator& Graph::Iterator::operator++() {
    if (i < graph->vertexCount) {
        ++i;
    }
    skipDeleted();
    return (*this);
}

Graph::Iterator Graph::Iterator::operator++(int) {
    Iterator temp(*this);
    if (i < graph->vertexCount) {
        ++i;
    }
    skipDeleted();
    return temp;
}

VertexNumber Graph::Iterator::operator*() {
    return i;
}

bool Graph::Iterator::operator!=(const Iterator& other) {
    return i != other.i;
}

bool Graph::Iterator::operator==(const Iterator& other) {
    return i == other.i;
}

void Graph::Iterator::skipDeleted() {
    while (i < graph->vertexCount && graph->deleted[i]) {
        ++i;
    }
}

Graph::Graph() : vertexCount(0), usedValue(0) {};

Graph::Graph(int vertexCount) : vertexCount(vertexCount), usedValue(0) {
    deleted.resize(vertexCount, false);
    neighbors.resize(vertexCount);
    vertexConstraints.resize(vertexCount);
}

Graph::Graph(int vertexCount, std::vector<Edge> edges) : Graph(vertexCount) {
    for (const Edge& edge : edges) {
        neighbors[edge.getStart()].push_back(edge.getEnd());
        neighbors[edge.getEnd()].push_back(edge.getStart());
    }

    for (VertexNumber vertex : (*this)) {
        std::sort(neighbors[vertex].begin(), neighbors[vertex].end());
    }
}

Graph::Iterator Graph::begin() {
    return Iterator(this, 0);
}

Graph::Iterator Graph::end() {
    return Iterator(this, vertexCount);
}

int Graph::getSize() {
    return std::distance(begin(), end());
}

int Graph::getVertexCount() const {
    return vertexCount;
}

int Graph::getEdgeCount() {
    int edgeCount = 0;
    for (auto vertex : (*this)) {
        edgeCount += getNeighbors(vertex).size();
    }

    return edgeCount;
}

bool Graph::containsEdge(VertexNumber u, VertexNumber v) {
    auto it = std::lower_bound(neighbors[u].begin(), neighbors[u].end(), v);
    return it != neighbors[u].end() && *it == v;
}

void Graph::addEdge(VertexNumber u, VertexNumber v) {
    neighbors[u].push_back(v);
    neighbors[v].push_back(u);
}

void Graph::deleteVertex(VertexNumber vertex) {
    deleted[vertex] = true;
    for (VertexNumber neighbor : getNeighbors(vertex)) {
        VertexList& secondNeighbors = neighbors[neighbor];
        secondNeighbors.erase(std::remove(secondNeighbors.begin(), secondNeighbors.end(), vertex), secondNeighbors.end());
    }
    neighbors[vertex].clear();
}

bool Graph::isDeleted(VertexNumber vertex) const {
    return deleted[vertex];
}

VertexList& Graph::getNeighbors(VertexNumber vertex) {
    return neighbors[vertex];
}

void Graph::sortNeighborhood(VertexNumber vertex) { 
	sort(neighbors[vertex].begin(), neighbors[vertex].end());
}

VertexList Graph::getClosedNeighborhood(VertexList& vertexSet) {
    VertexList neighborhood;
    for (int i = 0; i < vertexSet.size(); ++i) {
        neighborhood.push_back(vertexSet[i]);
        for (VertexNumber neighbor : getNeighbors(vertexSet[i])) {
            neighborhood.push_back(neighbor);
        }
    }
    sortAndRemoveDuplicates(neighborhood);
    return neighborhood;
}

VertexList Graph::getOpenNeighborhood(VertexList& vertexSet) {
    VertexList neighborhood;
    neighborhood = getClosedNeighborhood(vertexSet);
    sort(vertexSet.begin(), vertexSet.end());
    neighborhood = setMinus(neighborhood, vertexSet);
    return neighborhood;
}

bool Graph::isSubNeighborhood(VertexNumber u, VertexNumber v) {
    auto vNeighbors = getNeighbors(v);
    auto it = vNeighbors.begin();
    for (VertexNumber w : getNeighbors(u)) {
        if (w == v) {
            continue;
        }
        while (it != vNeighbors.end() && (*it) < w) {
            ++it;
        }
        if ((!(it != vNeighbors.end())) || (*it) != w) {
            return false;
        }
    }
    return true;
}


int Graph::getNeighborhoodEdgeCount(VertexNumber vertex) {
    int count = 0;
    for (VertexNumber neighbor1 : getNeighbors(vertex)) {
        for (VertexNumber neighbor2 : getNeighbors(vertex)) {
            if (neighbor2 == neighbor1) {
                break;
            }

            if (containsEdge(neighbor1, neighbor2)) {
                count++;
            }
        }
    }

    return count;
}

void Graph::addConstraint(const Constraint& constraint) {
    for (const Constraint::ConstraintTerm& term : constraint.terms) {
        vertexConstraints[term.vertex].push_back(constraints.size());
    }
    constraints.push_back(constraint);
}

void Graph::deleteVertexFromConstraints(VertexNumber vertex) {
    for (int constraintNumber : vertexConstraints[vertex]) {
        constraints[constraintNumber].deleteVertex(vertex);
    }
    vertexConstraints[vertex].clear();
}

void Graph::fixVertexInConstraints(VertexNumber vertex, bool inVC) {
    //TODO look for bad constraints
    for (int constraintNumber : vertexConstraints[vertex]) {
        constraints[constraintNumber].fixVertex(vertex, inVC);
    }
    vertexConstraints[vertex].clear();
}

void Graph::replaceVertexInConstraints(VertexNumber vertex, VertexNumber newVertex, bool flipped) {
    for (int constraintNumber : vertexConstraints[vertex]) {
        constraints[constraintNumber].replaceVertex(vertex, newVertex, flipped);
        vertexConstraints[newVertex].push_back(constraintNumber);
    }
    vertexConstraints[vertex].clear();
}

VertexNumber Graph::mergeVertices(VertexNumber u, VertexNumber v) {
    PROFILE_SECTION(mergeVertices);

    VertexList mergedNeighbors;
    mergedNeighbors.reserve(getNeighbors(u).size() + getNeighbors(v).size());
    mergedNeighbors.insert(mergedNeighbors.end(), getNeighbors(u).begin(), getNeighbors(u).end());
    mergedNeighbors.insert(mergedNeighbors.end(), getNeighbors(v).begin(), getNeighbors(v).end());
    sortAndRemoveDuplicates(mergedNeighbors);

    neighbors.emplace_back(std::move(mergedNeighbors));
    deleted.push_back(false);
    VertexNumber mergedVertex = vertexCount;
    labels.push_back(MERGED_LABEL);
    vertexConstraints.push_back({});
    vertexCount++;
    for (VertexNumber neighbor : neighbors[mergedVertex]) {
        neighbors[neighbor].push_back(mergedVertex);
    }

    deleteVertex(u);
    deleteVertex(v);

    return mergedVertex;
}

void Graph::setLabels(VertexList&& labels) {
    this->labels = labels;
}

VertexNumber Graph::getLabel(VertexNumber vertex) const {
    return labels[vertex];
}

void Graph::initUsed() {
    used.resize(vertexCount, 0);
    if (usedValue == 1000000000) {
        std::fill(used.begin(), used.end(), 0);
        usedValue = 0;
    }

    usedValue++;
}

bool Graph::isUsed(const VertexNumber vertex) const {
    assert(vertex < used.size());
    return used[vertex] == usedValue;
}

void Graph::setUsed(const VertexNumber vertex) {
    assert(vertex < used.size());
    used[vertex] = usedValue;
}

VertexList Graph::filterNotUsed(const VertexList& list) const {
    VertexList res;
    for (VertexNumber v : list) {
        if (!isUsed(v)) {
            res.push_back(v);
        }
    }

    return res;
}

void Graph::findReachable(VertexNumber vertex, VertexList& found) {
    setUsed(vertex);
    found.push_back(vertex);
    for (VertexNumber neighbor : getNeighbors(vertex)) {
        if (!isUsed(neighbor)) {
            findReachable(neighbor, found);
        }
    }
}

//vertices must be sorted!
Graph Graph::getInducedSubgraph(const VertexList& vertices) {
    PROFILE_SECTION(getInducedSubgraph);
    Graph res(vertices.size());
    int newIndex = 0;
    for (VertexNumber vertex : vertices) {
        int pos = 0;
        for (VertexNumber neighbor : getNeighbors(vertex)) {
            while (pos < vertices.size() && vertices[pos] < neighbor) {
                ++pos;
            }
            if (pos < vertices.size() && vertices[pos] == neighbor) {
                res.neighbors[newIndex].push_back(pos);
            }
        }
        newIndex++;
    }



    return res;
}

std::vector<Graph> Graph::splitOnComponents() {
    PROFILE_SECTION(splitOnComponents);
    initUsed();
    VertexList localNumber(vertexCount);
    std::vector<int> componentNumber(vertexCount);
    std::vector<Graph> components;
    for (VertexNumber root : (*this)) {
        if (!isUsed(root)) {
            VertexList component;
            findReachable(root, component);
            if (component.size() == 1) {
                continue;
            }

            std::sort(component.begin(), component.end());

            int cnt = 0;
            for (VertexNumber vertex : component) {
                localNumber[vertex] = cnt++;
                componentNumber[vertex] = components.size();
            }

            Graph subgraph(cnt);
            for (VertexNumber vertex : component) {
                for (VertexNumber neighbor : getNeighbors(vertex)) {
                    subgraph.neighbors[localNumber[vertex]].push_back(localNumber[neighbor]);
                }
            }

            subgraph.setLabels(std::move(component));

            components.push_back(subgraph);
        }
    }

    for (Constraint& constraint : constraints) {
        if (!constraint.isAlive()) {
            continue;
        }
        sort(constraint.terms.begin(), constraint.terms.end(), [&componentNumber](const Constraint::ConstraintTerm& a, const Constraint::ConstraintTerm& b)
        {
            return componentNumber[a.vertex] < componentNumber[b.vertex];
        });

        Constraint newConstraint;
        newConstraint.upperBound = constraint.upperBound;
        int prevComponent = -1;
        for (auto t : constraint.terms) {
            if (componentNumber[t.vertex] != prevComponent && prevComponent != -1) {
                if (newConstraint.terms.size() > constraint.upperBound) {
                    components[prevComponent].addConstraint(newConstraint);
                }
                newConstraint.terms.clear();
            } else {
                newConstraint.terms.push_back({localNumber[t.vertex], t.coefficient});
            }
            prevComponent = componentNumber[t.vertex];
        }
    }

    return components;
}


bool Graph::dfsMatching(VertexNumber vertex, VertexList& matching) {
    setUsed(vertex);
    for (VertexNumber neighbor : getNeighbors(vertex)) {
        if (matching[neighbor] == vertexCount || (!isUsed(matching[neighbor]) && dfsMatching(matching[neighbor], matching))) {
            matching[vertex] = neighbor;
            matching[neighbor] = vertex;
            return true;
        }
    }
    return false;
}

bool Graph::dfsLayeredMatching(VertexNumber vertex, VertexList& matching, const std::vector<int>& layer) {
    setUsed(vertex);
    for (VertexNumber neighbor : getNeighbors(vertex)) {
        VertexNumber next = matching[neighbor + vertexCount];
        if (next == INF || (layer[vertex] + 1 == layer[next] && !isUsed(next) && dfsLayeredMatching(next, matching, layer))) {
            matching[vertex] = neighbor + vertexCount;
            matching[neighbor + vertexCount] = vertex;
            return true;
        }
    }

    return false;
}

VertexList Graph::getMaxMatchingOnDuplicate() {
    PROFILE_SECTION(getMaxMatchingOnDuplicate);
    int N = 2 * vertexCount;
    VertexList matching(N, INF);
    std::vector<int> layer(vertexCount);

    bool changed;

    do {
        changed = false;

        initUsed();
        std::queue<VertexNumber> q;
        for (VertexNumber vertex : (*this)) {
            if (matching[vertex] == INF) {
                layer[vertex] = 0;
                q.push(vertex);
                setUsed(vertex);
            } else {
                layer[vertex] = INF;
            }
        }

        int maxLayer = INF;
        while (!q.empty()) {
            VertexNumber vertex = q.front();
            q.pop();
            for (VertexNumber neighbor : getNeighbors(vertex)) {
                VertexNumber next = matching[neighbor + vertexCount];
                if (next == INF) {
                    maxLayer = layer[vertex];
                    break;
                } else if (!isUsed(next)) {
                    setUsed(next);
                    layer[next] = layer[vertex] + 1;
                    q.push(next);
                }
            }
        }

        while (!q.empty()) {
            VertexNumber vertex = q.front();
            q.pop();
            if (layer[vertex] > maxLayer) {
                layer[vertex] = INF;
            }
        }

        initUsed();
        for (VertexNumber vertex : (*this)) {
            if (layer[vertex] == 0) {
                changed |= dfsLayeredMatching(vertex, matching, layer);
            }
        }
    } while (changed);

    return matching;
}

namespace {
    VertexList intersectListsWithUsed(const VertexList& a, const VertexList& b, const std::vector<bool>& used) {
        VertexList res;
        auto it = b.begin();
        for (auto u : a) {
            if (used[u]) {
                continue;
            }
            while (it != b.end() && (*it) < u) {
                ++it;
            }
            if (it != b.end() && (*it) == u) {
                res.push_back(u);
                ++it;
            }
        }

        return res;
    }
}

int Graph::getCliqueCoverLB() {
    PROFILE_SECTION(getCliqueCoverLB);
    int n = vertexCount;
    std::vector<std::pair<int, VertexNumber>> byDegree(n);
    int undeleted = 0;
    for (VertexNumber vertex = 0; vertex < n; ++vertex) {
        byDegree[vertex] = {deleted[vertex] ? 0 : getNeighbors(vertex).size(), vertex};
        if (!deleted[vertex] && getNeighbors(vertex).size() > 0) {
            undeleted++;
        }
    }
    std::sort(byDegree.begin(), byDegree.end());

    VertexList newIndex(n);
    for (VertexNumber vertex = 0; vertex < n; ++vertex) {
        newIndex[byDegree[vertex].second] = vertex;
    }
    std::vector<VertexList> g;
    for (VertexNumber vertex = 0; vertex < n; ++vertex) {
        VertexList neighbors;
        neighbors.reserve(byDegree[vertex].first);
        for (VertexNumber neighbor : getNeighbors(byDegree[vertex].second)) {
            neighbors.push_back(newIndex[neighbor]);
        }
        std::sort(neighbors.begin(), neighbors.end());
        g.emplace_back(std::move(neighbors));
    }

    std::vector<bool> usedInClique(n);
    int lowerBound = 0;
    for (VertexNumber v = 0; v < n; ++v) {
        if (usedInClique[v]) {
            continue;
        }

        usedInClique[v] = true;
        VertexList candidates;
        for (auto u : g[v]) {
            if (usedInClique[u]) {
                continue;
            }
            candidates.push_back(u);
        }

        int size = 1;
        while (candidates.size() > 0) {
            size++;
            int next = candidates[0];
            usedInClique[next] = true;

            candidates = intersectListsWithUsed(g[next], candidates, usedInClique);
        }
        lowerBound += size - 1;

    }

    return lowerBound;
}


int Graph::getCliqueCoverSmartLB() {
    PROFILE_SECTION(getCliqueCoverSmartLB);
    int n = vertexCount;
    std::vector<std::pair<int, VertexNumber>> byDegree(n);
    int undeleted = 0;
    for (VertexNumber vertex = 0; vertex < n; ++vertex) {
        byDegree[vertex] = {deleted[vertex] ? 0 : getNeighbors(vertex).size(), vertex};
        if (!deleted[vertex] && getNeighbors(vertex).size() > 0) {
            undeleted++;
        }
    }
    std::sort(byDegree.begin(), byDegree.end());

    VertexList newIndex(n);
    for (VertexNumber vertex = 0; vertex < n; ++vertex) {
        newIndex[byDegree[vertex].second] = vertex;
    }
    std::vector<VertexList> g;
    for (VertexNumber vertex = 0; vertex < n; ++vertex) {
        VertexList neighbors;
        neighbors.reserve(byDegree[vertex].first);
        for (VertexNumber neighbor : getNeighbors(byDegree[vertex].second)) {
            neighbors.push_back(newIndex[neighbor]);
        }
        std::sort(neighbors.begin(), neighbors.end());
        g.emplace_back(std::move(neighbors));
    }

    std::vector<bool> used(n);
    int lowerBound = 0;
    for (VertexNumber v = 0; v < n; ++v) {
        if (used[v]) {
            continue;
        }

        used[v] = true;
        VertexList candidates;
        for (auto u : g[v]) {
            if (used[u]) {
                continue;
            }
            candidates.push_back(u);
        }

        int size = 1;
        while (candidates.size() > 0) {
            size++;
            std::vector<VertexList> newCandidates(candidates.size());
            int best = -1;
            int mx = -1;
            for (int i = 0; i < candidates.size(); ++i) {
                VertexNumber u = candidates[i];
                newCandidates[i] = intersectListsWithUsed(g[u], candidates, used);
                if (int(newCandidates[i].size()) > mx) {
                    mx = newCandidates[i].size();
                    best = i;
                }
            }
            assert(best != -1);
            int next = candidates[best];
            used[next] = true;
            candidates = std::move(newCandidates[best]);
        }
        lowerBound += size - 1;

    }

    return lowerBound;
}

int Graph::getDenseCover(int sizec) {
    // compute a partition of the undeleted vertices of G into small dense graphs of fixed size
    PROFILE_SECTION(getDenseCover);
    int n = vertexCount;
    std::vector<std::pair<int, VertexNumber>> byDegree(n);
    std::vector<VertexNumber> newIndex(n);
    int undeleted = 0;
    
    for (VertexNumber vertex = 0; vertex < n; ++vertex) {
        byDegree[vertex] = {deleted[vertex] ? 0 : getNeighbors(vertex).size(), vertex};
    }
    std::sort(byDegree.begin(), byDegree.end());
    
    for (VertexNumber vertex = 0; vertex < n; ++vertex) {
        if (!deleted[vertex] && getNeighbors(vertex).size() > 0) {
            newIndex[byDegree[vertex].second] = undeleted;
            undeleted++;
        }
    }
    
    std::vector<std::vector<VertexNumber>> g;
    for (VertexNumber vertex = 0; vertex < n; ++vertex) {
        if (!deleted[vertex] && byDegree[vertex].first > 0) {
            std::vector<VertexNumber> neighbors;
            neighbors.reserve(byDegree[vertex].first);
            int actual_neighbors = 0;
            for (VertexNumber neighbor : getNeighbors(byDegree[vertex].second)) {
                if (!deleted[neighbor] && getNeighbors(neighbor).size() > 0) {
                    neighbors.push_back(newIndex[neighbor]);
                    actual_neighbors++;
                }
            }
            std::sort(neighbors.begin(), neighbors.end());
            g.emplace_back(std::move(neighbors));
        }
    }

    std::vector<bool> used(undeleted);
    for (auto i = 0; i < undeleted; ++i) {
        used[i]=false;
    }

    int num_used = 0;
    std::vector<int> num_neigh(undeleted);
    for (auto i = 0; i < undeleted; ++i) {
        num_neigh[i]=0;
    }

    std::vector<int> rank_dense(undeleted);
    for (auto i = 0; i < undeleted; ++i) {
        rank_dense[i]=-1;
    }
    
    int lowerBound = 0;
    for (VertexNumber v = 0; v < undeleted; ++v) {
        if (used[v]) {
            continue;
        }
        
        //begin build dense component of v
        std::vector<std::vector<VertexNumber>> dense_g(sizec);
        for (auto i = 0; i < sizec; ++i) {
            dense_g.reserve(sizec);
        }
        std::vector<VertexNumber> vertex_select(sizec);
        for (auto i = 0; i < sizec; ++i) {
            vertex_select[i]=-1;
        }
        int num_select = 0;
        int max_neigh = 0;
        std::vector<std::vector<VertexNumber>> part_nn(sizec+1);
        for (auto i = 0; i < sizec+1; ++i) {
            part_nn.reserve(sizec);
        }

        VertexNumber x;
        while ((num_select<sizec)&&(num_select+num_used<undeleted) ) {
            if (num_select==0) {
                x=v;
            } else {
                if (max_neigh==0) {
                    x=v;
                    while (used[x] || (rank_dense[x]!=-1)) {
                        x++;
                    }
                } else {
                    auto it3 = part_nn[max_neigh].begin();
                    x = *it3;
                    part_nn[max_neigh].erase(it3);
                    while ((part_nn[max_neigh].size()==0)&&(max_neigh>0)) {
                        max_neigh--;
                    }
                }
            }
            
            vertex_select[num_select] = x;
            rank_dense[x] = num_select;
            num_select++;
        
            for (auto u : g[x]) {
                if (used[u]) {
                    continue;
                }
                if (rank_dense[u]==-1) {
                    if (num_neigh[u]!=0) { 
                        bool found = false;
                        auto it = part_nn[num_neigh[u]].begin();
                        while ((!found) && (it != part_nn[num_neigh[u]].end())) {
                            if (*it == u) found = true;
                            else ++it;
                        }
                        if (found) part_nn[num_neigh[u]].erase(it);
                    }
                    
                    num_neigh[u]++;
                    if (num_neigh[u]>max_neigh) max_neigh=num_neigh[u];
                    
                    bool found = false;
                    int pos = 0;
                    auto it2 = part_nn[num_neigh[u]].begin();
                    while ((it2 != part_nn[num_neigh[u]].end())&&(!found)) {
                        if (*it2 > u) found = true;
                        else {++it2; pos++;}
                    }
                    if (pos < sizec) {
                        part_nn[num_neigh[u]].insert(it2,u);
                        if (part_nn[num_neigh[u]].size() >= sizec) part_nn[num_neigh[u]].pop_back();
                    }
                }
                else {
                    dense_g[rank_dense[u]].push_back(rank_dense[x]);
                }
            }
        }
        
        // update used, num_used and...
        for (auto i = 0; i < num_select; ++i) {
            for (auto u : g[vertex_select[i]]) {
                if (used[u]) {
                    continue;
                }
                if (rank_dense[u]==-1) num_neigh[u]--;
            }
        }
        
        for (auto i = 0; i < num_select; ++i) {
            used[vertex_select[i]]=true;
        }
        num_used += num_select;
        
        // build dense graph
        Graph graphdense = Graph(num_select);
        for (auto i = 0; i < num_select; ++i) {
            auto it = dense_g[i].begin();
            while (it != dense_g[i].end()) {
                graphdense.addEdge(i,*it);
                ++it;
            }
        }
        for (auto i = 0; i < num_select; ++i) {
            graphdense.sortNeighborhood(i);
        }
        
        Solver solverdense(std::move(graphdense));
        solverdense.solve();
        
        
        //end build dense component of v
        lowerBound += solverdense.getVCSize(); // += size vertex cover dense component v
    }

    return lowerBound;
}

void Graph::getMirrorsSatellitesAndConstraints(VertexNumber v, VertexList& mirrors, VertexList& satellites, std::vector<Constraint>& constraints) {
    PROFILE_SECTION(getMirrorsAndSatellites);
    VertexList neighbors, seconds;
    for (VertexNumber neighbor : getNeighbors(v)) {
        neighbors.push_back(neighbor);
        for (VertexNumber second : getNeighbors(neighbor)) {
            if (second != v) {
                seconds.push_back(second);
            }
        }
    }

    sortAndRemoveDuplicates(seconds);
    seconds = setMinus(seconds, getNeighbors(v));

    for (VertexNumber second : seconds) {
        VertexList remains = setMinus(neighbors, getNeighbors(second));
        auto secondNeighbors = getNeighbors(second);

        bool clique = true;
        for (int i = 0; i < remains.size() && clique; ++i) {
            for (int j = i + 1; j < remains.size() && clique; ++j) {
                if (!containsEdge(remains[i], remains[j])) {
                    clique = false;
                }
            }
        }
        
        if (clique) {
            mirrors.push_back(second);
        }
    }

    COUNT(thereAreMirrors, mirrors.size() > 0);
    
    if (mirrors.size() > 0) {
        return;
    }

    VertexList neighborsv = neighbors;
    neighborsv.push_back(v);
    sort(neighborsv.begin(), neighborsv.end());
    for (VertexNumber neighbor : neighbors) {
        auto a = setMinus(getNeighbors(neighbor), neighborsv);
        if (a.size() == 1) {
            satellites.push_back(a[0]);
        } else if (a.size() >= 2) {
            Constraint constraint;
            constraint.upperBound = int(a.size()) - 1;
            for (VertexNumber u : a) {
                constraint.terms.push_back({u, 1});
            }

            constraints.push_back(constraint);
        }
    }

    COUNT(thereAreSatellites, satellites.size() > 0);
}

bool Graph::isUnconfined(VertexNumber vertex) {
    auto vertexNeighbors = getNeighbors(vertex);
    VertexList S(1, vertex), SNeighbors(vertexNeighbors.begin(), vertexNeighbors.end());
    initUsed(); //vertices in N[S] are used
    for (VertexNumber v : SNeighbors) {
        setUsed(v);
    }
    setUsed(vertex);

    while (true) {
        VertexNumber toS = -1;
        for (VertexNumber u : SNeighbors) {
            int confined = -1;
            bool fail = false;
            for (VertexNumber parent : getNeighbors(u)) {
                if (!isUsed(parent)) {
                    if (confined == -1) {
                        confined = parent;
                    } else {
                        fail = true;
                        break;
                    }
                }
            }
            if (confined == -1) {
                return true;
            } else if (!fail) {
                toS = confined;
                break;
            }
        }

        if (toS == -1) {
            return false;
        }


        S.push_back(toS); 
        sort(S.begin(), S.end());
        auto toSNeighborsEL = getNeighbors(toS);
        VertexList toSNeighbors(toSNeighborsEL.begin(), toSNeighborsEL.end());
        SNeighbors = setMinus(SNeighbors, toSNeighbors);
        toSNeighbors = filterNotUsed(toSNeighbors);
        SNeighbors.insert(SNeighbors.end(), toSNeighbors.begin(), toSNeighbors.end());
        std::sort(SNeighbors.begin(), SNeighbors.end());

        setUsed(toS);
        for (VertexNumber v : toSNeighbors) {
            setUsed(v);
        }
    }
}
