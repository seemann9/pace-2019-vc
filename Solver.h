#pragma once

#include "Graph.h"

class Solver {
public:
    struct MergeDescriptor {
        VertexList conditionVertices, noVertices, yesVertices, fakeVertices;
    };
    Solver(const Graph& graph);
    Solver(Graph&& graph);
    bool solveBounded(int upperBound);
    void solve();
    int getVCSize() const;
    int getGraphSize() const;
    VertexList getVC() const;
    int removeLoopVertices();

private:
    Graph graph;
    VertexList vc;
    std::vector<MergeDescriptor> merges;
    int mergeVC;
    bool fail;

    void addToVC(VertexNumber vertex);
    void discardFromVC(VertexNumber vertex);
    void addVertexNeighborhoodToVC(VertexNumber vertex);
    void takeVCFrom(const Solver& other);
    void addConstraints(const std::vector<Constraint>& constraints);
    void addNeighborsConstraint(VertexNumber vertex, int upperBoundAddition);

    int getLowerBound();

    int removeIsolatedVertices();
    int removePendantVertices();
    int removeTriangles();
    int removeTwoPaths();
    int removeTwins();
    int removeAlternatives(VertexList& A, VertexList& B);
    int removeFunnel(int maxVertexNeighborhoodSize);
    int removeDesk();
    int removeUnconfinedVertices();
    int removeSubNeighborhoods();
    int removeSimplicialVertices();
    int removeCrown();
    int removeLP();
    int cycleCoverLBOfCycle(VertexList& cycle, int end);
    int cycleCoverLB();
    int checkConstraints();
    int preprocess();
    int kernelize();

    void setTrivialLabels();

    VertexNumber getBranchVertex();

    bool recursiveBranch(int upperBound);
    bool splitRecurse(std::vector<Solver>& componentSolvers, int lowerBound, std::vector<int>& componentLBs, int upperBound);
    std::vector<Solver> simplifyAndSplit();
};
