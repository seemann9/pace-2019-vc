#include <cstdio>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <ctime>

#include "Solver.h"
#include "Macro.h"
#include "Graph.h"

#ifdef DEBUG
FILE * logger = fopen("runner.log" , "a");
#endif

int main(int argc, char** argv) {
#ifdef DEBUG
    if (argc > 1) {
        fclose(logger);
        logger = fopen(argv[1] , "a");
    }
#endif
    std::srand(std::time(NULL));
    std::cin.sync_with_stdio(0);
    std::cin.tie(0);
    std::string s;
    int n, m;
    std::vector<Edge> edges;
    n = m = 0;
    while (std::getline(std::cin, s)) {
        if (s[0] == 'c') {
            continue;
        }
        if (s[0] == 'p') {
            std::stringstream ss(s.substr(5));
            ss >> n >> m;
            edges.reserve(m);
            continue;
        }
        std::stringstream ss(s);
        int u, v;
        ss >> u >> v;
        u--, v--;
        if (u > v) {
            std::swap(u, v);
        }
        edges.emplace_back(u, v);
    }

    std::sort(edges.begin(), edges.end());
    auto it = std::unique(edges.begin(), edges.end());
    edges.resize(std::distance(edges.begin(), it));

    Graph graph = Graph(n, edges);
    std::vector<VertexNumber> labels(n);
    for (VertexNumber vertex = 0; vertex < n; ++vertex) {
        labels[vertex] = vertex;
    }
    graph.setLabels(std::move(labels));

    Solver solver(std::move(graph));
    solver.removeLoopVertices();
    solver.solve();
    std::vector<VertexNumber> ans = solver.getVC();
    std::cout << "s vc " << n << ' ' << ans.size() << '\n';
    std::sort(ans.begin(), ans.end());
    for (auto v : ans) {
        std::cout << v + 1 << '\n';
    }

    return 0;
}
