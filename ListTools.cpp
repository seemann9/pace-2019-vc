#include <vector>
#include <algorithm>
#include "ListTools.h"

void sortAndRemoveDuplicates(VertexList& a) {
    sort(a.begin(), a.end());
    auto it = std::unique(a.begin(), a.end());
    a.resize(std::distance(a.begin(), it));
}

void sortAndRetainUnique(VertexList& a) {
    VertexList res;
    sort(a.begin(), a.end());
    bool prevEq = false;
    for (int i = 0; i < a.size(); ++i) {
        bool nextEq = (i + 1 < a.size() && a[i] == a[i + 1]);
        if (!prevEq && !nextEq) {
            res.push_back(a[i]);
        }

        prevEq = nextEq;
    }

    a = std::move(res);
}
