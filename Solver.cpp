#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <chrono>
#include <set>
#include <algorithm>
#include <cassert>
#include "Macro.h"
#include "Profiler.h"
#include "ListTools.h"
#include "Solver.h"
#include "ListTools.h"

Solver::Solver(const Graph& graph) : graph(graph), vc(), mergeVC(0), fail(false) {}
Solver::Solver(Graph&& graph) : graph(graph), vc(), mergeVC(0), fail(false) {}

void Solver::addToVC(VertexNumber vertex) {
    if (graph.isDeleted(vertex)) {
        return;
    }
    graph.fixVertexInConstraints(vertex, true);
    graph.deleteVertex(vertex);
    vc.push_back(vertex);
}

void Solver::discardFromVC(VertexNumber vertex) {
    if (graph.isDeleted(vertex)) {
        return;
    }
    graph.fixVertexInConstraints(vertex, false);
    graph.deleteVertex(vertex);
}

void Solver::takeVCFrom(const Solver& other) {
    auto otherVC = other.getVC();
    vc.insert(vc.end(), otherVC.begin(), otherVC.end());
}

int Solver::getVCSize() const {
    return vc.size() + mergeVC;
}

int Solver::getGraphSize() const {
    return graph.getVertexCount();
}

VertexList Solver::getVC() const {
    std::vector<bool> inVC(graph.getVertexCount());
    for (VertexNumber vertex : vc) {
        inVC[vertex] = true;
    }

    for (auto it = merges.rbegin(); it != merges.rend(); ++it) {
        bool condition = true;
        for (VertexNumber conditionVertex : it->conditionVertices) {
            if (!inVC[conditionVertex]) {
                condition = false;
            }
        }
        if (condition) {
            for (VertexNumber yesVertex : it->yesVertices) {
                inVC[yesVertex] = true;
            }
        } else {
            for (VertexNumber noVertex : it->noVertices) {
                inVC[noVertex] = true;
            }
        }

        for (VertexNumber fakeVertex : it->fakeVertices) {
            inVC[fakeVertex] = false;
        }
    }

    VertexList ans;
    for (VertexNumber vertex = 0; vertex < graph.getVertexCount(); ++vertex) {
        if (inVC[vertex]) {
            ans.push_back(graph.getLabel(vertex));
        }
    }

    return ans;
}

void Solver::addConstraints(const std::vector<Constraint>& constraints) {
    for (auto constraint : constraints) {
        graph.addConstraint(constraint);
    }
}

void Solver::addNeighborsConstraint(VertexNumber vertex, int upperBoundAddition) {
    Constraint newConstraint;
    for (VertexNumber neighbor : graph.getNeighbors(vertex)) {
        newConstraint.terms.push_back({neighbor, 1});
    }
    newConstraint.upperBound = int(newConstraint.terms.size()) - 1 + upperBoundAddition;
    graph.addConstraint(newConstraint);
}


int Solver::getLowerBound() {
    int cliqueLB = graph.getCliqueCoverSmartLB();
    int denseLB = graph.getDenseCover(graph.getVertexCount() / 2);
    COUNT(denseCoverBetter, denseLB > cliqueLB);
    COUNT(denseCoverWorse, denseLB < cliqueLB);
    return std::max(cliqueLB, denseLB);
}

int Solver::removeIsolatedVertices() {
    PROFILE_SECTION(removeIsolatedVertices);
    int deleted = 0;
    for (VertexNumber vertex : graph) {
        if (graph.getNeighbors(vertex).size() == 0) {
            deleted++;
            discardFromVC(vertex);
        }
    }
    COUNT(removeIsolatedVertices, deleted);
    return deleted;
}

int Solver::removeLoopVertices() {
    int deleted = 0;
    for (VertexNumber vertex : graph) {
        if (graph.containsEdge(vertex, vertex)) {
            deleted++;
            addToVC(vertex);
        }
    }
    return deleted;
}

int Solver::removePendantVertices() {
    PROFILE_SECTION(removePendantVertices);
    int deleted = 0;
    bool changed;
    do {
        changed = false;
        for (VertexNumber vertex : graph) {
            auto neighbors = graph.getNeighbors(vertex);
            if (neighbors.size() == 1) {
                changed = true;
                deleted += 2;
                addToVC(*neighbors.begin());
                discardFromVC(vertex);
            }
        }
    } while (changed);
    COUNT(removePendantVertices, deleted);
    return deleted;
}

int Solver::removeTriangles() {
    PROFILE_SECTION(removeTriangles);
    bool changed;
    int trianglesFound = 0;
    do {
        changed = false;
        for (VertexNumber vertex : graph) {
            auto neighbors = graph.getNeighbors(vertex);
            if (neighbors.size() == 2) {
                auto it = neighbors.begin();
                VertexNumber u = *it;
                ++it;
                VertexNumber v = *it;
                if (graph.containsEdge(u, v)) {
                    changed = true;
                    trianglesFound++;
                    discardFromVC(vertex);
                    addToVC(u);
                    addToVC(v);
                }
            }
        }
    } while (changed);
    COUNT(removeTriangles, trianglesFound * 3);
    return trianglesFound * 3;
}

int Solver::removeTwoPaths() {
    PROFILE_SECTION(removeTwoPaths);
    bool changed;
    int twoPathsFound = 0;
    int iter = 0;
    do {
        changed = false;
        for (auto it = graph.begin(); it != graph.end(); ++it) {
            VertexNumber vertex = *it;
            auto neighbors = graph.getNeighbors(vertex);
            if (neighbors.size() == 2) {
                auto it = neighbors.begin();
                VertexNumber u = *it;
                ++it;
                VertexNumber v = *it;
                if (!graph.containsEdge(u, v)) {
                    changed = true;
                    twoPathsFound++;
                    graph.deleteVertex(vertex);
                    VertexNumber mergedVertex = graph.mergeVertices(u, v);
                    graph.replaceVertexInConstraints(vertex, mergedVertex, true);
                    graph.replaceVertexInConstraints(u, mergedVertex, false);
                    graph.replaceVertexInConstraints(v, mergedVertex, false);
                    merges.push_back({{mergedVertex}, {vertex}, {u, v}, {mergedVertex}});
                    mergeVC++;
                }
            }
        }
    } while (changed);
    COUNT(removeTwoPaths, twoPathsFound * 2);
    return twoPathsFound * 2;
}

int Solver::removeTwins() {
    PROFILE_SECTION(removeTwins);
    bool changed;
    int twinsFound = 0;
    do {
        changed = false;
        for (auto it1 = graph.begin(); it1 != graph.end(); ++it1) {
            VertexNumber u = *it1;
            auto uNeighbors = graph.getNeighbors(u);
            if (uNeighbors.size() == 3) {
                auto it2 = uNeighbors.begin();
                VertexNumber x = *it2;
                auto xNeighbors = graph.getNeighbors(x);
                bool notwinfound = true;
                auto it3 = xNeighbors.begin();
                while ((it3 != xNeighbors.end()) && (notwinfound)) {
                    VertexNumber v = *it3;
                    if (v != u) {
                        auto vNeighbors = graph.getNeighbors(v);
                        if (vNeighbors.size() == 3) {
                            bool same = true;
                            auto it4 = vNeighbors.begin();
                            auto it6 = uNeighbors.begin();
                            while ((it4 != vNeighbors.end()) && (it6 != uNeighbors.end()) && same) {
                                if (*it4 != *it6) {same = false;}
                                ++it4; ++it6;
                            }
                            if (same) {
                                notwinfound = false;
                                changed = true;
                                auto it5 = uNeighbors.begin();
                                VertexNumber u1 = *it5; ++it5;
                                VertexNumber u2 = *it5; ++it5;
                                VertexNumber u3 = *it5;
                                
                                if (graph.containsEdge(u1,u2) || graph.containsEdge(u2,u3) || graph.containsEdge(u1,u3)) {
                                    // 1st case of reduction rule
                                    twinsFound += 5;
                                    discardFromVC(u); discardFromVC(v);
                                    addToVC(u1); addToVC(u2); addToVC(u3);
                                }
                                else {
                                    // 2nd case of reduction rule
                                    twinsFound += 4;
                                    graph.deleteVertex(u); graph.deleteVertex(v);
                                    VertexNumber merge1 = graph.mergeVertices(u2,u3);
                                    VertexNumber mergeVertex = graph.mergeVertices(u1,merge1);
                                    graph.replaceVertexInConstraints(u, mergeVertex, true);
                                    graph.replaceVertexInConstraints(v, mergeVertex, true);
                                    graph.replaceVertexInConstraints(u1, mergeVertex, false);
                                    graph.replaceVertexInConstraints(u2, mergeVertex, false);
                                    graph.replaceVertexInConstraints(u3, mergeVertex, false);

                                    merges.push_back({{mergeVertex}, {u, v}, {u1, u2, u3}, {mergeVertex}});
                                    mergeVC += 2;
                                }
                            }
                            
                        }
                        
                    }

                    if (notwinfound) ++it3;
                }
                
            }
            
        }
        
    } while (changed);

    COUNT(twinsFound, twinsFound);
    return twinsFound;
}


int Solver::removeSubNeighborhoods() {
    PROFILE_SECTION(removeSubNeighborhoods);
    int found = 0;
    for (auto it = graph.begin(); it != graph.end(); ++it) {
        VertexNumber vertex = *it;
        VertexList toVC;
        for (VertexNumber neighbor : graph.getNeighbors(vertex)) {
            if (graph.isSubNeighborhood(vertex, neighbor)) {
                toVC.push_back(neighbor);
                found++;
            }
        }

        for (VertexNumber neighbor : toVC) {
            addToVC(neighbor);
        }
    }

    COUNT(removeSubNeighborhoods, found);

    return found;
}

int Solver::removeAlternatives(VertexList& A, VertexList& B) { 
    PROFILE_SECTION(removeAlternatives);
    int deleted = 0;
    VertexList openA = graph.getOpenNeighborhood(A);
    VertexList openB = graph.getOpenNeighborhood(B);
    VertexList closedA = graph.getClosedNeighborhood(A);
    VertexList closedB = graph.getClosedNeighborhood(B);
    VertexList intersection = intersectLists(openA, openB);
    VertexList BminusA = setMinus(openB, closedA);
    VertexList AminusB = setMinus(openA, closedB);
    for (VertexNumber toRemove : A) { 
        graph.deleteVertexFromConstraints(toRemove);
        graph.deleteVertex(toRemove);
    }
    for (VertexNumber toRemove : B) { 
        graph.deleteVertexFromConstraints(toRemove);
        graph.deleteVertex(toRemove);
    }
    for (VertexNumber toRemove : intersection) { 
        addToVC(toRemove);
    }
    deleted += A.size() + B.size() + intersection.size();

    std::vector<std::pair<VertexNumber, VertexNumber>> edgesToAdd;
    for (VertexNumber v1 : AminusB) { 
        for (VertexNumber v2 : BminusA) { 
            if (!graph.containsEdge(v1, v2)) {
                edgesToAdd.push_back({v1, v2});
            }
        }
    }

    for (auto newEdge: edgesToAdd) { 
        graph.addEdge(newEdge.first, newEdge.second);
    }

    for (VertexNumber vertex : AminusB) {
        graph.sortNeighborhood(vertex);
    }

    for (VertexNumber vertex : BminusA) {
        graph.sortNeighborhood(vertex);
    }

    merges.push_back({BminusA, B, A, {}});
    mergeVC += A.size();
    return deleted;
}

int Solver::removeFunnel(int maxVertexNeighborhoodSize) {
    PROFILE_SECTION(removeFunnelVertices);
    int found = 0;
    for (auto it = graph.begin(); it != graph.end(); ++it) {
        VertexNumber vertex = *it;
        auto neighbors = graph.getNeighbors(vertex);
        if (neighbors.size() == 0 || neighbors.size() > maxVertexNeighborhoodSize) { 
            continue;
        }
        bool clique = true;
        bool existsPotential = false;
        bool uniquePotential = false;
        VertexNumber candidate1;
        VertexNumber candidate2;
        auto it2 = neighbors.begin();
        while ((it2 != neighbors.end()) && (clique)) {
            VertexNumber firstNeighbor = *it2;
            auto it3 = neighbors.begin();
            while ((it3 != it2) && (clique)) {
                VertexNumber secondNeighbor = *it3;
                if (!graph.containsEdge(firstNeighbor, secondNeighbor)) {
                    if (!existsPotential) {
                        existsPotential = true;
                        candidate1 = firstNeighbor;
                        candidate2 = secondNeighbor;
                    } else if (!uniquePotential) {
                        bool independentEdge = true;
                        for (VertexNumber a : {firstNeighbor, secondNeighbor} ) {
                            for (VertexNumber b : {candidate1, candidate2} ) {
                                if (a == b) {
                                    independentEdge = false;
                                    uniquePotential = true;
                                    candidate1 = a;
                                }
                            }
                        }
                        if (independentEdge) { 
                            clique = false; 
                            break;
                        }
                    } else {
                        bool independentEdge = true;
                        for (VertexNumber a : {firstNeighbor, secondNeighbor} ) {
                            if (a == candidate1) {
                                independentEdge = false;
                            }

                        }
                        if (independentEdge) { 
                            clique = false; 
                            break;
                        }
                    }
                }
                ++it3;
            }
            ++it2;
        }
        if (!existsPotential) {
            candidate1 = *neighbors.begin();
        }
        if (clique) {
            VertexList altA = VertexList({vertex});
            VertexList altB = VertexList({candidate1});
            found += removeAlternatives(altA, altB);
        }		       
    }
    COUNT(removeFunnel, found);
    return found;
}

int Solver::removeDesk() {
    PROFILE_SECTION(removeDeskVertices);
    int found = 0;
    for (auto it = graph.begin(); it != graph.end(); ++it) {
        VertexNumber vertex = *it;
        bool changed =false;
        auto neighbors = graph.getNeighbors(vertex);
        if ((neighbors.size() > 4) || (neighbors.size() < 3)) {
            continue;
        }
        auto it2 = neighbors.begin();
        while ((it2 != neighbors.end()) && (!changed)) {
            VertexNumber b1 = *it2;
            ++it2;
            auto b1Neighbors = graph.getNeighbors(b1);
            if ((b1Neighbors.size() > 4) || (b1Neighbors.size() < 3)) {
                continue;
            }
            auto it3 = it2;
            while ((it3 != neighbors.end()) && (!changed)) {
                VertexNumber b2 = *it3;
                ++it3;
                auto b2Neighbors = graph.getNeighbors(b2);
                if (graph.containsEdge(b1, b2) || (b2Neighbors.size() > 4) || (b2Neighbors.size() < 3)) {
                    continue;
                }
                for (VertexNumber a2 : b1Neighbors) {
                    if ((a2 == vertex) || graph.containsEdge(vertex, a2)) {
                        continue;
                    }
                    auto a2Neighbors = graph.getNeighbors(a2);
                    if ((a2Neighbors.size() > 4) || (a2Neighbors.size() < 3)) {
                        continue;
                    }
                    if (graph.containsEdge(a2, b2)) {
                        VertexList candA = VertexList({vertex, a2});
                        VertexList candB = VertexList({b1, b2});
                        std::sort(candA.begin(), candA.end());
                        std::sort(candB.begin(), candB.end());
                        VertexList openA = graph.getOpenNeighborhood(candA);
                        VertexList openB = graph.getOpenNeighborhood(candB);						
                        if ((intersectLists(openA,openB).size() == 0) && 
                                (setMinus(openA, candB).size() < 3) && 
                                (setMinus(openB, candA).size() < 3)) {
                            found += removeAlternatives(candA, candB);
                            changed = true;
                            break;							
                        }
                    }
                }
            }
        }	

    }
    COUNT(removeDesk, found);    
    return found;	
}

int Solver::removeUnconfinedVertices() {
    PROFILE_SECTION(removeUnconfinedVertices);
    int found = 0;
    for (auto it = graph.begin(); it != graph.end(); ++it) {
        VertexNumber vertex = *it;
        if (graph.isUnconfined(vertex)) {
            addToVC(vertex);
            found++;
        }
    }
    COUNT(removeUnconfined, found);
    return found;
}

int Solver::removeSimplicialVertices() {
    PROFILE_SECTION(removeSimplicialVertices);
    int found = 0;
    bool changed = false;
    do {
        changed = false;
        for (auto it = graph.begin(); it != graph.end(); ++it) {
            VertexNumber vertex = *it;
            VertexList neighbors;
            for (VertexNumber neighbor : graph.getNeighbors(vertex)) {
                neighbors.push_back(neighbor);
            }
            bool clique = true;
            for (int i = 0; i < neighbors.size(); ++i) {
                for (int j = 0; j < i; ++j) {
                    if (!graph.containsEdge(neighbors[i], neighbors[j])) {
                        clique = false;
                        break;
                    }
                }
                if (!clique) {
                    break;
                }
            }
            if (clique) {
                changed = true;
                found++;
                discardFromVC(vertex);
                for (VertexNumber neighbor : neighbors) {
                    found++;
                    addToVC(neighbor);
                }
            }
        }
    } while (changed);

    COUNT(removeSimplicialVertices, found);

    return found;
}


int Solver::removeCrown() {
    PROFILE_SECTION(removeCrown);
    int n = graph.getVertexCount();
    VertexList vertices;
    for (VertexNumber vertex : graph) {
        vertices.push_back(vertex);
    }
    std::random_shuffle(vertices.begin(), vertices.end());
    VertexList globalMatching(n, n);
    for (VertexNumber vertex : vertices) {
        if (globalMatching[vertex] < n) {
            continue;
        }
        for (VertexNumber neighbor : graph.getNeighbors(vertex)) {
            if (globalMatching[neighbor] == n) {
                globalMatching[neighbor] = vertex;
                globalMatching[vertex] = neighbor;
                break;
            }
        }
    }

    VertexList unmatched;
    for (VertexNumber vertex : graph) {
        if (globalMatching[vertex] == n) {
            unmatched.push_back(vertex);
        }
    }

    graph.initUsed();
    VertexList localMatching(n, n);
    int matchingSize = 0;
    for (VertexNumber vertex : unmatched) {
        if (graph.dfsMatching(vertex, localMatching)) {
            graph.initUsed();
            matchingSize++;
        }
    }

    graph.initUsed();
    for (VertexNumber vertex : unmatched) {
        if (localMatching[vertex] == n) {
            graph.dfsMatching(vertex, localMatching);
        }
    }

    int deleted = 0;
    std::set<VertexNumber> head;
    for (VertexNumber vertex : vertices) {
        if (graph.isUsed(vertex)) {
            for (VertexNumber neighbor : graph.getNeighbors(vertex)) {
                head.insert(neighbor);
            }
            deleted++;
            discardFromVC(vertex);
        }
    }

    for (VertexNumber vertex : head) {
        deleted++;
        addToVC(vertex);
    }

    COUNT(removeCrown, deleted);

    return deleted;
}

int Solver::removeLP() {
    PROFILE_SECTION(removeLP);

    int n = graph.getVertexCount();
    
    VertexList matching = graph.getMaxMatchingOnDuplicate();

    int deleted = 0;

    for (VertexNumber vertex : graph) {
        bool isDuplicateUsed = (matching[vertex + n] < INF && graph.isUsed(matching[vertex + n]));
        if (graph.isUsed(vertex) && !isDuplicateUsed) {
            deleted++;
            discardFromVC(vertex);
        } else if (!graph.isUsed(vertex) && isDuplicateUsed) {
            deleted++;
            addToVC(vertex);
        }
    }	

    COUNT(removeLP, deleted);
    return deleted;
}


int Solver::cycleCoverLBOfCycle(VertexList& cycle, int end) {
    int lb = 0; 
    int size = cycle.size();
    bool clique = true;
    for (int i = 0; i < size; ++i) {
        for (int j = 0; j < i; ++j) {
            if (!graph.containsEdge(cycle[i], cycle[j])) {
                clique = false;
                break;
            }
        }
        if (!clique) {
            break;
        }
    }
    if (clique) {
        return cycle.size() - 1;
    }
    for (int i = 0; i < end; i++) {
        for (int j = i+1; j < end; j++) { 
            if (graph.containsEdge(cycle[i], cycle[(j+1)%size]) && graph.containsEdge(cycle[i+1], cycle[j])) {
                VertexList cycle1;
                VertexList cycle2;
                for (int k = j+1; k < size + i+1; k++) {
                    cycle1.push_back(cycle[k%size]);
                }
                lb += cycleCoverLBOfCycle(cycle1, end-j-1);
                for (int k= i+1; k < j+1; k++) {
                    cycle2.push_back(cycle[k]);
                }
                lb += cycleCoverLBOfCycle(cycle2, cycle2.size());
                return lb;
            }
        }
    }
    lb = size/2 + size%2;
    return lb;
}
 


int Solver::cycleCoverLB() {
    PROFILE_SECTION(cycleCoverLB);
    int n = graph.getVertexCount();

    VertexList matching = graph.getMaxMatchingOnDuplicate();

    int lb = 0;

    std::vector<bool> inCycleCover(n, false);
    for (VertexNumber vertex : graph) {
        std::cout.flush();
        if (!inCycleCover[vertex]) {
            VertexList nextCycle;
            nextCycle.push_back(vertex);
            inCycleCover[vertex] = true;
            VertexNumber matchedVertex = matching[vertex];
            while (vertex != matchedVertex - n) {
                VertexNumber nextVertex = matchedVertex - n;
                inCycleCover[nextVertex] = true;
                nextCycle.push_back(nextVertex);
                matchedVertex = matching[nextVertex];
            }
            lb += cycleCoverLBOfCycle(nextCycle,nextCycle.size());
        }
    }
    return lb;
}

int Solver::checkConstraints() {
    PROFILE_SECTION(checkConstraints);
    int deleted = 0;
    
    for(int constraintIndex = 0; constraintIndex < graph.constraints.size(); ++constraintIndex) {
        Constraint& constraint = graph.constraints[constraintIndex];
        
        if (constraint.isAlive()) {
            VertexList yes, no;
            for (auto term : constraint.terms) {
                if (term.coefficient == 1) {
                    no.push_back(term.vertex);
                } else {
                    yes.push_back(term.vertex);
                }
            }

            sortAndRemoveDuplicates(no);
            if (constraint.upperBound <= 0) {
                constraint.terms.clear();
                for (VertexNumber v : no) {
                    for (VertexNumber neighbor : graph.getNeighbors(v)) {
                        yes.push_back(neighbor);
                    }
                }
                sortAndRemoveDuplicates(yes);
                if (intersectLists(no, yes).size() > 0) {
                    fail = true;
                    break;
                }

                VertexList noClosedNeighborhood = graph.getClosedNeighborhood(no);
                VertexList noOpenNeighborhood = setMinus(noClosedNeighborhood, no);
                for (VertexNumber u : noOpenNeighborhood) {
                    VertexList intersection = intersectLists(graph.getNeighbors(u), no);
                    if (intersection.size() == 1) {
                        Constraint newConstraint;
                        for (VertexNumber v : setMinus(graph.getNeighbors(u), noClosedNeighborhood)) {
                            newConstraint.terms.push_back({v, 1});
                        }
                        if (newConstraint.terms.size() > 0) {
                            newConstraint.upperBound = int(newConstraint.terms.size()) - 1;
                            graph.addConstraint(newConstraint);
                        }
                    }
                }

                for (VertexNumber v : no) {
                    deleted++;
                    discardFromVC(v);
                }

                for (VertexNumber v : yes) {
                    deleted++;
                    addToVC(v);
                }
            } else if (no.size() > constraint.upperBound) {
                int upperBound = constraint.upperBound;
                VertexList noOpenNeighborhood = graph.getOpenNeighborhood(no);
                for (VertexNumber neighbor : noOpenNeighborhood) {
                    if (intersectLists(graph.getNeighbors(neighbor), no).size() > upperBound) {
                        deleted++;
                        addNeighborsConstraint(neighbor, -1);
                        addToVC(neighbor);
                    }
                }

                if (no.size() > 2 * upperBound) {
                    Solver inducedSolver(std::move(graph.getInducedSubgraph(no)));
                    //TODO check LBs only
                    if (!inducedSolver.solveBounded(upperBound)) {
                        fail = true;
                        break;
                    }
                }
            }
        }
    }

    COUNT(checkConstraintsFails, fail);
    COUNT(checkConstraints, deleted);
    return deleted;
}

int Solver::preprocess() {
    PROFILE_SECTION(preprocess);
    int ans = 0, oldAns = 0;
    do {
        oldAns = ans;
        ans += removeIsolatedVertices();
        ans += removePendantVertices();
        ans += removeTriangles();
        ans += removeTwoPaths();
        //ans += removeSimplicialVertices();
    } while (ans > oldAns);
    return ans;
}

int Solver::kernelize() {
    PROFILE_SECTION(kernelize);
    int ans = 0, oldAns = 0;
    do {
        oldAns = ans;
        ans += preprocess();
        //ans += removeCrown();        
        ans += removeFunnel(50);

        if (ans == oldAns) {
            ans += removeSubNeighborhoods();
        }
        if (ans == oldAns) {
            ans += removeUnconfinedVertices();
        }
        /*
        if (ans == oldAns) {
            ans += removeTwins();
        }
        if (ans == oldAns) {
            ans += removeDesk();
        }
        if (ans == oldAns) {
            ans += removeLP();
        }
        */
        ans += checkConstraints();
        if (fail) {
            return ans;
        }
    } while (ans > oldAns);

    return ans;
}

void Solver::setTrivialLabels() {
    VertexList labels(graph.getVertexCount());
    for (VertexNumber vertex = 0; vertex < graph.getVertexCount(); ++vertex) {
        labels[vertex] = vertex;
    }
    graph.setLabels(std::move(labels));
}

void Solver::addVertexNeighborhoodToVC(VertexNumber vertex) {
    VertexList copyNeighbors = graph.getNeighbors(vertex);
    for (VertexNumber neighbor : copyNeighbors) {
        addToVC(neighbor);
    }

    discardFromVC(vertex);
}

std::vector<Solver> Solver::simplifyAndSplit() {
    PROFILE_SECTION(simplifyAndSplit);
    kernelize();
    std::vector<Solver> componentSolvers;
    if (fail) {
        return componentSolvers;
    }
    std::vector<Graph> components = graph.splitOnComponents();

    for (auto component : components) {
        componentSolvers.emplace_back(std::move(component));
    }

    return componentSolvers;
}

bool Solver::splitRecurse(std::vector<Solver>& componentSolvers, int lowerBound, std::vector<int>& componentLBs, int upperBound) {
    PROFILE_SECTION(splitRecurse);
    for (int i = 0; i < componentSolvers.size(); ++i) {
        Solver& componentSolver = componentSolvers[i];
        lowerBound -= componentLBs[i];
        if (!componentSolver.recursiveBranch(upperBound - lowerBound)) {
            return false;
        }

        lowerBound += componentSolver.getVCSize();
        if (lowerBound > upperBound) {
            return false;
        }

        takeVCFrom(componentSolver);
    }

    return true;
}

VertexNumber Solver::getBranchVertex() {
    PROFILE_SECTION(getBranchVertex);
    VertexNumber res = -1;
    int maxDegree = -1;
    int minEdges = 0;

    for (VertexNumber vertex : graph) {
        int degree = graph.getNeighbors(vertex).size();
        if (degree < maxDegree) {
            continue;
        }
        int edges = graph.getNeighborhoodEdgeCount(vertex);
        if (degree > maxDegree || edges < minEdges) {
            res = vertex;
            maxDegree = degree;
            minEdges = edges;
        }
    }

    return res;
}

bool Solver::recursiveBranch(int upperBound) {
    //TODO refactor so Solver.fail and return value are unified
    PROFILE_SECTION(recursiveBranch);
    upperBound -= getVCSize(); //should be run with empty VC anyway

//    VertexNumber branchVertex = *std::max_element(graph.begin(), graph.end(), [this](VertexNumber u, VertexNumber v){return this->graph.getNeighbors(u).size() < this->graph.getNeighbors(v).size();});
    VertexNumber branchVertex = getBranchVertex();

    VertexList mirrors, satellites;
    std::vector<Constraint> constraints;
    graph.getMirrorsSatellitesAndConstraints(branchVertex, mirrors, satellites, constraints);

    Solver solver1(graph);
    solver1.setTrivialLabels();
    solver1.addNeighborsConstraint(branchVertex, 0);
    solver1.addToVC(branchVertex);
    
    for (auto mirror : mirrors) {
        solver1.addToVC(mirror);
    }
    auto componentSolvers1 = solver1.simplifyAndSplit();

    int lowerBound1 = std::numeric_limits<int>::max();
    std::vector<int> componentLBs1;
    if (!solver1.fail) {
        lowerBound1 = solver1.getVCSize();
        for (Solver& componentSolver : componentSolvers1) {
            int componentLB = componentSolver.getLowerBound();
            componentLBs1.push_back(componentLB);
            lowerBound1 += componentLB;
        }
    }

    Solver solver2(graph);
    solver2.setTrivialLabels();
    solver2.addConstraints(std::move(constraints));
    solver2.addVertexNeighborhoodToVC(branchVertex);
    if (mirrors.size() == 0) {
        bool satelliteEdge = false;
        for (int i = 0; i < satellites.size() && !satelliteEdge; ++i) {
            for (int j = i + 1; j < satellites.size() && !satelliteEdge; ++j) {
                if (graph.containsEdge(satellites[i], satellites[j])) {
                    satelliteEdge = true;
                }
            }
        }
        if (satelliteEdge) {
            bool success1 = (!solver1.fail) && solver1.splitRecurse(componentSolvers1, lowerBound1, componentLBs1, upperBound);
            if (success1) {
                takeVCFrom(solver1);
            }
            return success1;
        }
        for (VertexNumber satellite : satellites) {
            solver2.addVertexNeighborhoodToVC(satellite);
        }
    }

    auto componentSolvers2 = solver2.simplifyAndSplit();
    int lowerBound2 = std::numeric_limits<int>::max();
    std::vector<int> componentLBs2;
    if (!solver2.fail) {
        lowerBound2 = solver2.getVCSize();
        for (Solver& componentSolver : componentSolvers2) {
            int componentLB = componentSolver.getLowerBound();
            componentLBs2.push_back(componentLB);
            lowerBound2 += componentLB;
        }
    }

    if (lowerBound1 > lowerBound2) {
        std::swap(lowerBound1, lowerBound2);
        std::swap(componentLBs1, componentLBs2);
        std::swap(solver1, solver2);
        std::swap(componentSolvers1, componentSolvers2);
    }

    if (lowerBound1 > upperBound || solver1.fail) {
        return false;
    }

    int oldUpperBound = upperBound;
    bool success1 = solver1.splitRecurse(componentSolvers1, lowerBound1, componentLBs1, upperBound);
    if (success1) {
        upperBound = std::min(upperBound, solver1.getVCSize() - 1);
        if (lowerBound2 > upperBound || solver2.fail) {
            takeVCFrom(solver1);
            return true;
        }
    } else {
        if (lowerBound2 > upperBound || solver2.fail) {
            return false;
        }
    }

    bool success2 = solver2.splitRecurse(componentSolvers2, lowerBound2, componentLBs2, upperBound);
    if (!success1 && !success2) {
        return false;
    } else if (success1 && !success2) {
        takeVCFrom(solver1);
    } else if (!success1 && success2) {
        takeVCFrom(solver2);
    } else {
        if (solver1.getVCSize() <= solver2.getVCSize()) {
            takeVCFrom(solver1);
        } else {
            takeVCFrom(solver2);
        }
    }

    TO_LOG("Branch: size %d, ub %d, lb1 %d, lb2 %d, vc1 %d, vc2 %d\n", graph.getVertexCount(), oldUpperBound, lowerBound1, lowerBound2, solver1.getVCSize(), solver2.getVCSize());
    return true;
}

bool Solver::solveBounded(int upperBound) {
    PROFILE_SECTION(solveBounded);
    auto componentSolvers = simplifyAndSplit();
    if (fail) {
        return false;
    }
    std::vector<int> componentLBs(componentSolvers.size());
    int lowerBound = getVCSize();
    for (int i = 0; i < componentSolvers.size(); ++i) {
        componentLBs[i] = componentSolvers[i].getLowerBound();
        lowerBound += componentLBs[i];
    }

    if (lowerBound > upperBound) {
        return false;
    }

    for (int i = 0; i < componentSolvers.size(); ++i) {
        Solver& componentSolver = componentSolvers[i];
        lowerBound -= componentLBs[i];
        bool result = componentSolver.recursiveBranch(upperBound - lowerBound);
        if (!result) {
            return false;
        }

        lowerBound += componentSolver.getVCSize();

        if (lowerBound > upperBound) {
            return false;
        }

        takeVCFrom(componentSolver);
    }

    COUNT(solveBoundedOK, 1);
    return true;
}

void Solver::solve() {
    PROFILE_SECTION(solve);
    auto componentSolvers = simplifyAndSplit();
    assert(!fail);
    for (Solver& componentSolver : componentSolvers) {
        bool result = componentSolver.recursiveBranch(componentSolver.getGraphSize() - 1);
        assert(result);
        takeVCFrom(componentSolver);
    }
}

