#pragma once

#include <vector>
#include "VertexNumber.h"

class Constraint {
    public:
    struct ConstraintTerm {
        VertexNumber vertex;
        int coefficient;//0 or 1
    };

    bool isAlive() const;

    void deleteVertex(VertexNumber vertex);
    void fixVertex(VertexNumber vertex, bool inVC);
    void replaceVertex(VertexNumber vertex, VertexNumber newVertex, bool flipped);

    std::vector<ConstraintTerm> terms;
    int upperBound;
};
