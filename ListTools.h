#pragma once

#include <vector>
#include "Graph.h"

template <class Iterable1, class Iterable2>
VertexList setMinus(const Iterable1& a, const Iterable2& b) {
    VertexList res;
    auto it = b.begin();
    for (VertexNumber v : a) {
        while (it != b.end() && (*it) < v) {
            ++it;
        }
        if ((!(it != b.end())) || (*it) > v) {
            res.push_back(v);
        }
    }

    return res;
}

template <class Iterable1, class Iterable2>
VertexList intersectLists(const Iterable1& a, const Iterable2& b) {
        VertexList res;
        auto it = b.begin();
        for (auto u : a) {
            while (it != b.end() && (*it) < u) {
                ++it;
            }
            if (it != b.end() && (*it) == u) {
                res.push_back(u);
                ++it;
            }
        }

        return res;
}

void sortAndRemoveDuplicates(VertexList& a);

void sortAndRetainUnique(VertexList& a);
